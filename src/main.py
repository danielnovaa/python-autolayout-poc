import random
import networkx as nx
import matplotlib.pyplot as plt

reds = []
blues = []
yellows = []

lines = 0


def has_crossover(node1, node2):
    return (node1 in reds and node2 in yellows) or (node1 in yellows and node2 in reds)


def share_category(node1, node2):
    in_reds = node1 in reds and node2 in reds
    in_blues = node1 in blues and node2 in blues
    in_yellows = node1 in yellows and node2 in yellows
    return in_reds or in_blues or in_yellows


def setup():
    G = nx.Graph()
    for i in reds:
        G.add_node(i, color='red')
    for i in blues:
        G.add_node(i, color='blue')
    for i in yellows:
        G.add_node(i, color='yellow')
    nodes = list(G.nodes)
    for i in range(lines):
        node1 = random.choice(nodes)
        node2 = random.choice(nodes)
        while node2 == node1 or has_crossover(node1, node2):
            node2 = random.choice(nodes)
        if share_category(node1, node2):
            weight = 3
        else:
            weight = 1
        G.add_edge(node1, node2, weight=weight, color='black')
    for i in blues:
        for j in range(i+1, max(blues)+1):
            if not (i, j) in G.edges:
                G.add_edge(i, j, weight=3, color='white')
    for i in reds:
        for j in range(i+1, max(reds)+1):
            if not (i, j) in G.edges:
                G.add_edge(i, j, weight=3, color='white')
    for i in yellows:
        for j in range(i+1, max(yellows)+1):
            if not (i, j) in G.edges:
                G.add_edge(i, j, weight=3, color='white')
    color_map = []
    for _ in reds:
        color_map.append('red')
    for _ in blues:
        color_map.append('blue')
    for _ in yellows:
        color_map.append('yellow')
    edge_colors = []
    for edge in G.edges:
        edge_colors.append(G[edge[0]][edge[1]]['color'])
    pos = nx.random_layout(G)
    nx.draw(G, pos, edge_color=edge_colors, node_color=color_map, with_labels=True)
    plt.show()
    pos = nx.bipartite_layout(G, blues, scale=3)
    nx.draw(G, pos, edge_color=edge_colors, node_color=color_map, with_labels=True)
    plt.show()
    pos = nx.spring_layout(G)
    nx.draw(G, pos, edge_color=edge_colors, node_color=color_map, with_labels=True)
    plt.show()


def main():
    global lines, reds, blues, yellows
    print("Input the number of red nodes: ")
    n_reds = int(input())
    print("Input the number of blue nodes: ")
    n_blues = int(input()) + n_reds
    print("Input the number of yellow nodes: ")
    n_yellows = int(input()) + n_blues
    print("Input the number of lines: ")
    lines = int(input())
    reds = range(n_reds)
    blues = range(n_reds, n_blues)
    yellows = range(n_blues, n_yellows)
    setup()
main()

